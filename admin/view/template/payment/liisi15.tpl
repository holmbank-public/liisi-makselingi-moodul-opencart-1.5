<?php echo $header; ?>
<div id="content">
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><?php echo $entry_field_status; ?></td>
            <td><select name="liisi_status" id="liisi_status" class="form-control">
                <option value="0" <?php echo ($liisi_status == 0 ? 'selected="selected"' : ''); ?>>Disabled</option>
                <option value="1" <?php echo ($liisi_status == 1 ? 'selected="selected"' : ''); ?>>Enabled</option>
              </select></td>
          </tr>        
          <tr>
            <td><?php echo $entry_field_server; ?></td>
            <td><select name="liisi_server" id="liisi_server" class="form-control">
                <option value="0" <?php echo ($liisi_server == 0 ? 'selected="selected"' : ''); ?>>Test</option>
                <option value="1" <?php echo ($liisi_server == 1 ? 'selected="selected"' : ''); ?>>Live</option>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_field_snd; ?></td>
            <td><input type="text" name="liisi_snd_id" id="liisi_snd_id" value="<?php echo $liisi_snd_id; ?>" placeholder="<?php echo $entry_field_snd; ?>" class="form-control" /></td>
          </tr>
          <tr>
            <td><?php echo $entry_field_public; ?></td>
            <td><textarea name="liisi_public" id="liisi_public" class="form-control"><?php echo $liisi_public; ?></textarea></td>
          </tr>
          <tr>
            <td><?php echo $entry_field_private; ?></td>
            <td><textarea name="liisi_private" id="liisi_private" class="form-control"><?php echo $liisi_private; ?></textarea></td>
          </tr>
          <tr>
            <td><?php echo $entry_field_private_password; ?></td>
            <td><input type="password" name="liisi_private_password" id="liisi_private_password" value="<?php echo $liisi_private_password; ?>" placeholder="<?php echo $liisi_private_password; ?>" class="form-control" /></td>
          </tr>
          <tr>
            <td><?php echo $entry_field_payment_confirm; ?></td>
            <td><input type="text" name="liisi_payment_confirm" id="liisi_payment_confirm" value="<?php echo $liisi_payment_confirm; ?>" placeholder="<?php echo $entry_field_payment_confirm; ?>" class="form-control" /></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="liisi_sort_order" value="<?php echo $liisi_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" /></td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?> 